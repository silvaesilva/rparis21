
# rparis21 <img src="man/figures/logo.png" align="right" width="13%"/>

<!-- badges: start -->
<!-- [![R build status](https://github.com/rnabioco/valr/workflows/R-CMD-check/badge.svg)](https://github.com/rnabioco/valr/actions) -->
<!-- [![codecov](https://codecov.io/gh/rnabioco/valr/branch/master/graph/badge.svg)](https://codecov.io/gh/rnabioco/valr) -->
<!-- [![](https://www.r-pkg.org/badges/version/valr)](https://CRAN.R-project.org/package=valr) -->
<!-- badges: end -->

The `rparis21` package provides all tools to compute the Paris21
Indicator. The package was designed to work internally in the
[DataLab](http://www.fao.org/datalab/website/) infrastructure. This
project was developed jointly between OECD and FAO-ESS teams.

-   **Text extraction**: `rparis21` enables text extraction from PDF,
    Word, and also digitalized documents through the libraries [Tika
    Apache](https://docs.ropensci.org/rtika/) and
    [pdftools](https://docs.ropensci.org/pdftools/).

-   **Language Detection**: the language detection procedure is made by
    the [Compact Language Detector](https://github.com/google/cld3)
    proposed by Google company. CLD is a neural network model for
    language identification.

## Installation

The latest stable version can be installed from Bitbucket:

``` r
remotes::install_bitbucket(repo = "silvaesilva/rparis21", ref = "generalized", upgrade = "never")
```

## Example

See the `vignette("setup_file")` for further information about the setup
file, and the columns required for the tables keywords and rule.

``` r
library(yaml)
library(rparis21)

SETTINGS <- yaml::yaml.load_file("setup.yml")

fpath <- system.file("extdata", "paris21_job.R", package = "rparis21")
source(fpath)

out_p21 <- paris21_job(SETTINGS = SETTINGS)
```

The example above may be wrapped in a R file and then executed by the
`Rscript` command,

    ~$ Rscript run_paris21_job.R >& log_paris21.log &

where `run_paris21_job.R` is file with the R commands described above,
and `log_paris21.log` shows the output messages sent by the script
during the execution. The log file is used to check if the scripts is
working well.
