#' @title Classify sentences into Paris 21 levels index
#'
#' @param sentence_df the data.frame exported by the function \link{text_to_sentences}. The data.frame is structured as:
#' * __id_sentence:__ a unique ID for each sentence.
#' * __n_words:__ number of words in the sentence.
#' * __sentences:__ the sentence.
#' * __language:__ the language used in the sentence.
#' @param rule_df a data.frame with the instructions to compute the levels:
#' * __levels:__ the level name. It must be unique name.
#' * __rule:__ the rule associated to each level. It must be a logical R expression. The user
#' must use __is.na(CLASS) for NOT CONTAIN__ that class, and __!is.na(CLASS) for CONTAIN__ that class.
#' For example: is.na(stats) & !is.na(numbers) means sentences that DO NOT contain keywords from STATS class,
#' but CONTAIN the NUMBERS.
#' @param keywords_df the keywords data.frame with the following columns:
#' * __language:__ the language of the keyword. The users can translate their keywords to any language as needed.
#' * __keyword:__ keyword string
#' * __keyword_stem:__ keywords stemmed
#' * __class:__ the keywords' class
#' @param n_words number valid words per sentences. The sentence with less than __n_words__ will be removed.
#' @param n_cores number cores used in the parallel process.
#' @param parallel Logical. If the value is TRUE, so the a parallel computation is performed.
#'
#' @return A data.frame in the long format with all keywords were detected in each sentence.
#' * __id_sentence:__ the same unique ID used in the input.
#' * __<CLASS>:__ one column for each class of the keywords. Each row stores the keyword found. NA means that no one keyword was detected.
#' * __<level_X>:__ one column for each level of the indicator.
#'
#' @importFrom  stringr str_detect str_remove_all str_squish
#' @importFrom parallel detectCores
#' @importFrom utils data
#' @importFrom rlang parse_quo !! := caller_env
#' @import dplyr foreach doParallel
#' @md
#' @export
classifier_sentences <- function(sentence_df,
                                 keywords_df,
                                 rule_df,
                                 n_words = 3,
                                 n_cores = 2,
                                 parallel = FALSE#,
                                 # method = 'fao'
                                 ) {

  if(missing(rule_df)) stop("The parameter rule_df is required")
  if(missing(keywords_df)) stop("The parameter keywords_df is required")
  if(missing(sentence_df)) stop("The parameter sentence_df is required")

  required_cols_keywords <- c("language", "keyword", "keyword_stem", "class")
  required_cols_sentence <- c("id_sentence")
  required_cols_rule <- c("levels", "rule")

  if(!all(required_cols_keywords %in% names(keywords_df))) stop("The keyword data.frame must contain the variables: language, keyword, keyword_stem, class")
  if(!all(required_cols_sentence %in% names(sentence_df)) | ncol(sentence_df) < 2) stop("The sentence_df data.frame must contain the variables: id_sentence and at least a class and a level.")
  if(!all(required_cols_rule %in% names(rule_df))) stop("The rule_df data.frame must contain the variables: levels and rule")

  if(nrow(sentence_df) == 0 | nrow(rule_df)  == 0 | nrow(keywords_df) == 0) stop("One of the data.frames rule_df, sentence_df or keywords_df is empty.")

  if(anyDuplicated(rule_df$levels)) stop("The levels column must have unique names.")
  if(anyDuplicated(rule_df$rule)) stop("The rule column must have unique rules.")

  method = "fao"
  #it reduces repeated whitespace inside a string, and also the the whitespace from start and end of string
  #it build a regex to look up the sentences
  if(method == "oecd") {
    keywords_df <- keywords_df %>%
      mutate(keywords_str = paste0(" ", str_squish(keyword_stem), " "))

    sentence_df <- sentence_df %>%
      mutate(sentences = paste0(" ", str_squish(sentences), " "))
  } else {

    keywords_df <- keywords_df %>%
      mutate(keywords_str = paste0("\\b", str_squish(keyword_stem), "\\b"))

  }

  ##-- Cleaning the sentences ----
  p_n_words <- n_words
  sentence_df <- sentence_df %>%
    filter(n_words >= p_n_words) %>%
    arrange(id_sentence)

  ## Remove numbers at intial of a string.
  sentence_df <- sentence_df %>%
    mutate(sentences = str_squish(str_remove_all(sentences, "^(\\d+(.|\\s)?)+")))
    # mutate(sentences = str_squish(str_remove_all(sentences, "(^[0-9]+)")))

  p_n_tot_sentences <- n_distinct(sentence_df$id_sentence)
  keywords_split <- split(keywords_df, keywords_df$class)

  # TODO: how to handle year and numbers at the same time. For example, if the user
  # want to detect number and also years?
  # For now, I do not consider that scenario, so I remove the numbers which fall into
  # the pattern \\b([1][9][5-9][0-9]|[2][0][0-4][0-9])\\b. This regex gets all years between
  # 1950 and 2049. However, if there is a number like 2000Kg, it will be removed.
  text_df <- sentence_df
  text_df$sentences <- str_replace_all(text_df$sentences, "\\b([1][9][5-9][0-9]|[2][0][0-4][0-9])\\b", "")

  keywords_groups <- unique(keywords_df$class)

  keywords_detected_list <- detect_keywords(p_doc = text_df,
                                            p_keywords = keywords_df,
                                            p_group = keywords_groups)

  level_df <- Reduce(function(x, y) full_join(x, y, by = "id_sentence"),
                     keywords_detected_list)

  level_df <- level_df %>%
    mutate_at(.vars = vars(starts_with('key')), .funs = function(x) gsub("\\\\b", "", x)) %>%
    mutate(n_tot_sentences = p_n_tot_sentences)

    ## OECD method
    # if(method == "oecd") {
    #   level_df <- level_df %>%
    #     mutate(level3_fs = !is.na(key_fs) & !is.na(key_math) & !is.na(key_disagg),
    #            # level2_fs = !is.na(key_fs) & contain_number == TRUE & contain_year == FALSE & !is.na(key_disagg),
    #            level2_fs = !is.na(key_fs) & contain_number == TRUE & !is.na(key_disagg),
    #            level1_fs = !is.na(key_fs) & !is.na(key_disagg),
    #            level3_sdgs = !is.na(key_sdg) & !is.na(key_math) & !is.na(key_disagg),
    #            level2_sdgs = !is.na(key_sdg) & contain_number == TRUE & contain_year == FALSE & !is.na(key_disagg),
    #            level1_sdgs = !is.na(key_sdg) & !is.na(key_disagg))
    # }

    # if(method == "fao") {
      level_df <- level_df %>%
        rename_with(~gsub("key_", "", .x))

      for(i in 1:nrow(rule_df)) {
        level_df <- level_df %>%
          mutate(!!rule_df$levels[i] := !!parse_quo(tolower(rule_df$rule[i]), env = caller_env()))
      }
    # }


  return(level_df)
}


#' Classify sentences into Paris 21 levels index (__BATCH__)
#'
#' @param input a path directory where the sentences files are stored.
#' @param output a path directory to save the levels computed for each document.
#' @param rule_df a data.frame with the instructions to compute the levels:
#' * __levels:__ the level name. It must be unique name.
#' * __rule:__ the rule associated to each level. It must be a logical R expression. The user
#' must use __is.na(CLASS) for NOT CONTAIN__ that class, and __!is.na(CLASS) for CONTAIN__ that class.
#' For example: is.na(stats) & !is.na(numbers) means sentences that DO NOT contain keywords from STATS class,
#' but CONTAIN the NUMBERS.
#' @param languages the character vector with the languages to be considered in the algorithm.
#' @param restart logical. The default is TRUE that means the process will restart from the last document processed.
#' For FALSE, the process will restart from the begining again.
#' @inheritParams classifier_sentences
#'
#' @return The directory where the files are stored.
#'
#' @importFrom usethis ui_todo ui_done
#'
#' @md
#' @export
classifier_sentences_batch <- function(input,
                                       output,
                                       rule_df,
                                       keywords_df,
                                       languages,
                                       restart = FALSE,
                                       n_words = 3,
                                       n_cores = 2,
                                       parallel = FALSE) {

  method = 'fao'

  if(missing(input)) stop("The parameter input is required")
  if(missing(output)) stop("The parameter output is required")
  if(missing(rule_df)) stop("The parameter rule_df is required")
  if(missing(keywords_df)) stop("The parameter keywords_df is required")

  required_cols_keywords <- c("language", "keyword", "keyword_stem", "class")
  required_cols_rule <- c("levels", "rule")

  if(!all(required_cols_keywords %in% names(keywords_df))) stop("The keyword data.frame must contain the variables: language, keyword, keyword_stem, class")
  if(!all(required_cols_rule %in% names(rule_df))) stop("The rule_df data.frame must contain the variables: levels and rule")

  if(nrow(rule_df)  == 0 | nrow(keywords_df) == 0) stop("One of the data.frames rule_df, sentence_df or keywords_df is empty.")

  if(anyDuplicated(rule_df$levels)) stop("The levels column must have unique names.")
  if(anyDuplicated(rule_df$rule)) stop("The rule column must have unique rules.")

  if(!dir.exists(output)) dir.create(output, recursive = TRUE)

  # if restart  == FALSE --> then restart the process from the
  # last document processed, otherwise, restart the process from
  # the begining by overwriting all files.
  if(!restart) {
    docs_id_processed <- list.files(output)
    docs_id_processed <- gsub("^(levels_)|(.rds)$", "", docs_id_processed)

    docs_id <- gsub("(.rds)$", "", list.files(input))

    docs_id_NO_processed <- docs_id[!docs_id %in% docs_id_processed]

    sentences_files <- file.path(input, paste0(docs_id_NO_processed, ".rds"))
    n_sentences_files <- length(sentences_files)

  } else {

    sentences_files <- list.files(input, full.names = TRUE)
    n_sentences_files <- length(sentences_files)

  }

  usethis::ui_todo('Paris21 classification has been started')

  for(i in seq_along(sentences_files)) {

    text_df <- readRDS(sentences_files[i])

    language_name <- unique(text_df$language)

    if(!language_name %in% languages) next

    keywords_doc <- keywords_df %>%
      filter(tolower(language) == language_name)

    levels_df <- classifier_sentences(sentence_df = text_df,
                                      keywords_df = keywords_doc,
                                      rule_df = rule_df,
                                      parallel = parallel,
                                      n_cores = n_cores,
                                      n_words = n_words)

    docname <- gsub("\\.rds$", "", basename(sentences_files[i]))

    levels_df <- levels_df %>%
      mutate(document = docname)

    saveRDS(object = levels_df, file = file.path(output, paste0('levels_', docname, ".rds")))
    usethis::ui_done(paste0(basename(sentences_files)[i], "  |------->  ", scales::percent(i/n_sentences_files), " (", i, ")", "  |----->  ", Sys.time()))

  }

  return(output)

}

