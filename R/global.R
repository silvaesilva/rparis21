utils::globalVariables(
  c("cld2_name" , "cld2_prop" , "cld3_name" , "cld3_prop" , "contain_number" , "contain_year",
    "detectCores" , "i" , "id_sentence" , "indicator" , "is_word" , "l1" ,
    "l2", "l3" , "name" , "pdf_path" , "sentences" , "str_which" ,
    "text","word" , "word_stem", "data", "keywords", "language_code", "doc", ".",
    "language", "to_remove", "text_in_sentences_dir",
    "keyword_stem", "document", "value", "n_sentences_tot", "freq", "prop", "index_outlier",
    "weight", "SL_NAME_COLLECTION" ,"path_basename_s", "language_s", "content_txt" ,"subject_ss",
    "descriptor_ss", "isocountry_ss", "txt_files", "keywords_stem", "n_tot_sentences",
    "keywords_permutation", "keyword", "keywords_tidy"
    )
)
